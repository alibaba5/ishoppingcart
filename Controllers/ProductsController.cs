﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iShoppingCart.Models;
using Microsoft.AspNetCore.Mvc;

namespace iShoppingCart.Controllers
{
    public class ProductsController : Controller
    {
        [Route("Products")]
        [Route("Products/Index")]
        [Route("Products/Index/{id?}")]
        public IActionResult Index()
        {
            var model = new ProductsViewModel().getAllProducts();
            return View(model);
        }
    }
}