﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iShoppingCart.Repositories.Interfaces;
using iShoppingCart.Entities;

namespace iShoppingCart.Repositories
{
    public class Repository<T> : IRepository<Product> where T : class
    {
        public IEnumerable<Product> GetAll()
        {
            LinkedList<Product> productsList = new LinkedList<Product>(); ;
            
            for (int i= 0; i < 10; i++) {
                Product product = new Product();
                product.id = i;
                product.name = "Product # " + i;
                productsList.AddFirst(product);
            }

            return productsList;
        }
    }
}
