﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iShoppingCart.Entities;
using iShoppingCart.Repositories;
using iShoppingCart.Services;

namespace iShoppingCart.Models
{
    public class ProductsViewModel
    {
        public IEnumerable<Product>  getAllProducts() { return new ProductsViewModelService().GetAll(); }
                
    }
}
